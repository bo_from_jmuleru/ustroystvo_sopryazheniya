PCBNEW-LibModule-V1  Чт 30 июл 2015 09:48:37
# encoding utf-8
Units mm
$INDEX
smd dip8
$EndINDEX
$MODULE smd dip8
Po 0 0 0 15 55B99074 00000000 ~~
Li smd dip8
Sc 0
AR 
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "smd dip8"
T1 0 3.81 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "6" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 1.27
$EndPAD
$PAD
Sh "7" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 -1.27
$EndPAD
$PAD
Sh "5" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 3.81
$EndPAD
$PAD
Sh "8" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 -3.81
$EndPAD
$PAD
Sh "1" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.08 -3.81
$EndPAD
$PAD
Sh "2" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.08 -1.27
$EndPAD
$PAD
Sh "3" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.08 1.27
$EndPAD
$PAD
Sh "4" R 2.54 1.27 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.08 3.81
$EndPAD
$EndMODULE smd dip8
$EndLIBRARY
